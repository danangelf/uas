"""my_web URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path
from .views import index
from .views import index, DataDetailView, DataCreateView #baru
from .views import DataEditView, DataDeleteView, DataToPdf

urlpatterns = [
    path('', index, name='home_page'),
    path('pengontrolan/<int:pk>', DataDetailView.as_view(),
         name='data_detail_view'),
    path('pengontrolan/add', DataCreateView.as_view(), name='data_add'),
    path('pengontrolan/edit/<int:pk>', DataEditView.as_view(), name='data_edit'),
    path('pengontrolan/delete/<int:pk>', DataDeleteView.as_view(),
         name='data_delete'),
    path('pengontrolan/print_pdf', DataToPdf.as_view(),name='data_to_pdf')
]
