from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse #baru
from django.utils import timezone

# Create your models here.
class Pengontrolan(models.Model):
    KATEGORI_CHOICES = (
        ('kb','Kurang Baik'),
        ('bk','Baik'),
        ('bg','Bagus'),
    )
    nama_pemilik = models.CharField('Nama Inkubator', max_length=50, null=False)
    tgl_input = models.DateTimeField('Tanggal',default=timezone.now)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    kategori = models.CharField(max_length=2, choices=KATEGORI_CHOICES)
    tinggi = models.FloatField('Tinggi (cm)')
    debit = models.FloatField('Debit (ml/detik)')

    class Meta:
        ordering = ['-tgl_input']

    def __str__(self):
        return self.nama_pemilik

    def get_absolute_url(self): #baru
        return reverse('home_page')