from .models import Pengontrolan # baru gaes
from django.shortcuts import render
from django.views.generic import DetailView, CreateView
from django.views.generic import UpdateView
from django.urls import reverse_lazy
from django.views.generic import DeleteView
from .utils import Render
from django.views.generic import View#baru
# Create your views here.
var = {
    'judul' : 'Website Monitoring Water Flow Inkubator Telur Ikan Nila',
     'info' : '''Kami menyediakan informasi terkait kondisi Waterflow pada Inkubator peternak yang telah terdaftar''',
     'oleh' : 'DANANG DAN ALI'
}
def index(self):
    #baru gaes .....
    var['pengontrolan'] = Pengontrolan.objects.values('id','nama_pemilik','kategori').\
         order_by('nama_pemilik')
    return render(self, 'pengontrolan/index.html',context=var)

class DataDetailView(DetailView): #baru gaes
    model = Pengontrolan
    template_name = 'pengontrolan/data_detail_view.html'

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context
		
class DataCreateView(CreateView): #baru
    model = Pengontrolan
    fields = '__all__'
    template_name = 'pengontrolan/data_add.html'

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context
		
class DataEditView(UpdateView):
    model = Pengontrolan
    fields = ['nama_pemilik','kategori','tinggi',
              'debit']
    template_name = 'pengontrolan/data_edit.html'

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context

class DataDeleteView(DeleteView):
    model = Pengontrolan
    template_name = 'pengontrolan/data_delete.html'
    success_url = reverse_lazy('home_page')

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context
		
class DataToPdf(View):
    def get(self,request):
        var = {
            'pengontrolan' : Pengontrolan.objects.values(
                'nama_pemilik','kategori','tinggi','debit'),
            'request':request
        }
        return Render.to_pdf(self,'pengontrolan/data_to_pdf.html',var)
		
		
		
		
		
		
		