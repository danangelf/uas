from django.contrib import admin
from .models import Pengontrolan

# Register your models here.
@admin.register(Pengontrolan)
class PengontrolanAdmin(admin.ModelAdmin):
    list_display = ['nama_pemilik','tgl_input','user','kategori',
                    'tinggi','debit']
    list_filter = ['nama_pemilik','kategori','tgl_input','user']
    search_fields = ['nama_pemilik','kategori','user']